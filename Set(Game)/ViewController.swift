import UIKit

class ViewController: UIViewController {
    private let game = GameSet()
    
    @IBOutlet private var cardsOnTable: [UIButton]!
    @IBOutlet weak var cardsLeftLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for card in cardsOnTable {
            paintCard(card)
        }
        game.scoreChangedHandler = { self.scoreLabel.text = "Score: \(self.game.score)" }
        game.setOfCardsResetedHandler = {
            //var timer: Timer
                       
            for card in self.cardsOnTable {
                card.layer.borderWidth = 1
            }
            self.updateFromModel()
        }
        updateFromModel()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        let index = cardsOnTable.index(of: sender)!
        game.cardSelectedHandler = { sender.layer.borderWidth = 3 }
        game.selectCard(number: index)
    }
    
    @IBAction private func addThreeCards(_ sender: UIButton) {
        game.addThreeCardsOnTable()
        cardsLeftLabel.text = "\(game.cardsInDeckLeft) cards in the deck left"
        updateFromModel()
    }
    
    @IBAction func restartGame(_ sender: UIButton) {
        game.restart()
        updateFromModel()
    }
    
    private func paintCard(_ card: UIButton) {
            card.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            card.layer.borderWidth = 1
            card.layer.cornerRadius = 8
    }
    
    private func updateFromModel() {
        for i in game.cardsOnTable.indices {
            if let cardFromModel = game.cardsOnTable[i] {
                printCard(card: cardFromModel)
                let attTitle = NSAttributedString(
                    string: getSymbolByCardPropeties(cardFromModel),
                    attributes: getCardNSAttributes(cardFromModel))
                cardsOnTable[i].setAttributedTitle(attTitle, for: .normal)
            }
            else {
                let attTitle = NSAttributedString(string: ".")
                cardsOnTable[i].setAttributedTitle(attTitle, for: .normal)
            }
        }
    }
    
    //MARK:- Support functions
    
    private func printCardsArray(arr: [Card]) {
        for card in arr {
            print(card.symbolCount, card.symbolType, card.color, card.patternType)
        }
    }
    
    private func printCard(card: Card) {
        print(card.symbolCount, card.symbolType, card.color, card.patternType)
    }
    
    private func getCardNSAttributes(_ card: Card) -> [NSAttributedStringKey: Any] {
        var color: UIColor
        
        switch card.color {
        case .red:
            color = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        case .green:
            color = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case .purple:
            color = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        }
        
        var attributeDictionary: [NSAttributedStringKey: Any] = [:]
        
        switch card.patternType {
        case .filled:
            attributeDictionary[.foregroundColor] = color.withAlphaComponent(1)
            attributeDictionary[.strokeWidth] = -1
        case .striped:
            attributeDictionary[.foregroundColor] = color.withAlphaComponent(0.15)
        case .outline:
            attributeDictionary[.foregroundColor] = color.withAlphaComponent(1)
            attributeDictionary[.strokeWidth] = 5
            attributeDictionary[.strokeColor] = color.withAlphaComponent(1)
        }
        
        return attributeDictionary
    }
    
    private func getSymbolByCardPropeties(_ card: Card) -> String {
        var cardsSymbol: String
        
        switch card.symbolCount {
        case .one:
            switch card.symbolType {
            case .ellipse:
                cardsSymbol = "●"
            case .rhombus:
                cardsSymbol = "◆"
            case .wave:
                cardsSymbol = "∿"
            }
        case .two:
            switch card.symbolType {
            case .ellipse:
                cardsSymbol = "●●"
            case .rhombus:
                cardsSymbol = "◆◆"
            case .wave:
                cardsSymbol = "∿∿"
            }
        case .three:
            switch card.symbolType {
            case .ellipse:
                cardsSymbol = "●●●"
            case .rhombus:
                cardsSymbol = "◆◆◆"
            case .wave:
                cardsSymbol = "∿∿∿"
            }
        }
        return cardsSymbol
    }
}

class Card: Equatable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.symbolCount == rhs.symbolCount && lhs.symbolType == rhs.symbolType && lhs.color == rhs.color && lhs.patternType == rhs.patternType
    }
    
    enum CardsSymbolType: Int {
        case ellipse = 0
        case rhombus
        case wave
        static var allCases: [CardsSymbolType] = [.ellipse, .rhombus, .wave]
    }
    
    enum CardsColor: Int {
        case red = 0
        case green
        case purple
        static var allCases: [CardsColor] = [.red, .green, .purple]
    }
    
    enum CardsSymbolCount: Int {
        case one = 0
        case two
        case three
        static var allCases: [CardsSymbolCount] = [.one, .two, .three]
    }
    
    enum CardsPatternType: Int {
        case outline = 0
        case filled
        case striped
        static var allCases: [CardsPatternType] = [.outline, .filled, .striped]
    }
    
    var id: Int
    
    var symbolCount: CardsSymbolCount
    var symbolType: CardsSymbolType
    var color: CardsColor
    var patternType: CardsPatternType
    
    var isMatched = false
    
    init(symbolCount: CardsSymbolCount, symbolType: CardsSymbolType, color: CardsColor, patternType: CardsPatternType)  {
        self.symbolCount = symbolCount
        self.symbolType = symbolType
        self.color = color
        self.patternType = patternType
        id = 0
    }
}

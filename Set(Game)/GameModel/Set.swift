import Foundation

class GameSet {
    private var deck: [Card] = []
    private(set) var cardsOnTable = [Card?]()
    private(set) var score = 0 {
        didSet {
            scoreChangedHandler?()
        }
    }
    
    private var setOfThreeCards = [Card]() {
        didSet {
            print(setOfThreeCards.count)
            if setOfThreeCards.count == 0 {
                setOfCardsResetedHandler?()
            }
            if setOfThreeCards.count == 3 {
                print(checkForSet(setOfThreeCards[0], setOfThreeCards[1], setOfThreeCards[2]))
            }
        }
    }
    
    var cardsInDeckLeft: Int {
        return deck.count
    }
    
    var scoreChangedHandler: (() -> Void)?
    var cardSelectedHandler: (() -> ())?
    var setOfCardsResetedHandler: (() -> ())?
    
    init() {
        formDeck()
        shuffleDeck()
        layOutCardsOnTable()
    }
    
    private func formDeck() {
        for symbolCount in 0..<Card.CardsSymbolCount.allCases.count {
            for symbolType in 0..<Card.CardsPatternType.allCases.count {
                for color in 0..<Card.CardsColor.allCases.count {
                    for patternType in 0..<Card.CardsPatternType.allCases.count {
                        deck.append(Card(
                            symbolCount: Card.CardsSymbolCount.allCases[symbolCount],
                            symbolType: Card.CardsSymbolType.allCases[symbolType],
                            color: Card.CardsColor.allCases[color],
                            patternType: Card.CardsPatternType.allCases[patternType]))
                    }
                }
            }
        }
    }
    
    private func layOutCardsOnTable() {
        for index in 0...11 {
            cardsOnTable.append(deck[index])
        }
        for _ in 0...11 {
            cardsOnTable.append(nil)
        }
        deck.removeSubrange(0...11)
    }
    
    private func shuffleDeck() {
        var cardsForShuffle = deck
        deck = []
        for _ in 0..<cardsForShuffle.count {
            let index = cardsForShuffle.count.arc4random
            deck += [cardsForShuffle.remove(at: index)]
        }
    }
    
    func addThreeCardsOnTable() {
        guard cardsInDeckLeft > 0 else {return}
        
        var count = 0
        for index in cardsOnTable.indices {
            if cardsOnTable[index] == nil {
                cardsOnTable[index] = deck[0]
                deck.remove(at: 0)
                count += 1
            }
            if count == 3 {
                break
            }
        }
    }
    
    func checkForSet(_ firstCard: Card, _ secondCard: Card, _ thirdCard: Card) -> Bool {
        var isSet = false
        if (firstCard.symbolCount == secondCard.symbolCount && secondCard.symbolCount == thirdCard.symbolCount) &&
            (firstCard.symbolType == secondCard.symbolType && secondCard.symbolType == thirdCard.symbolType) &&
            (firstCard.color == secondCard.color && secondCard.color == thirdCard.color) &&
            !(firstCard.patternType == secondCard.patternType || secondCard.patternType == thirdCard.patternType || firstCard.patternType == thirdCard.patternType) ||
            
            (firstCard.symbolCount == secondCard.symbolCount && secondCard.symbolCount == thirdCard.symbolCount) &&
            (firstCard.symbolType == secondCard.symbolType && secondCard.symbolType == thirdCard.symbolType) &&
            !(firstCard.color == secondCard.color || secondCard.color == thirdCard.color || firstCard.color == thirdCard.color) &&
            (firstCard.patternType == secondCard.patternType && secondCard.patternType == thirdCard.patternType) ||
            
            (firstCard.symbolCount == secondCard.symbolCount && secondCard.symbolCount == thirdCard.symbolCount) &&
            !(firstCard.symbolType == secondCard.symbolType || secondCard.symbolType == thirdCard.symbolType || firstCard.symbolType == thirdCard.symbolType) &&
            (firstCard.color == secondCard.color && secondCard.color == thirdCard.color) &&
            (firstCard.patternType == secondCard.patternType && secondCard.patternType == thirdCard.patternType) ||
            
            !(firstCard.symbolCount == secondCard.symbolCount || secondCard.symbolCount == thirdCard.symbolCount || firstCard.symbolCount == thirdCard.symbolCount) &&
            (firstCard.symbolType == secondCard.symbolType && secondCard.symbolType == thirdCard.symbolType) &&
            (firstCard.color == secondCard.color && secondCard.color == thirdCard.color) &&
            (firstCard.patternType == secondCard.patternType && secondCard.patternType == thirdCard.patternType) ||
            
            !(firstCard.symbolCount == secondCard.symbolCount || secondCard.symbolCount == thirdCard.symbolCount || firstCard.symbolCount == thirdCard.symbolCount) &&
            !(firstCard.symbolType == secondCard.symbolType || secondCard.symbolType == thirdCard.symbolType || firstCard.symbolType == thirdCard.symbolType) &&
            !(firstCard.color == secondCard.color || secondCard.color == thirdCard.color || firstCard.color == thirdCard.color) &&
            !(firstCard.patternType == secondCard.patternType || secondCard.patternType == thirdCard.patternType || firstCard.patternType == thirdCard.patternType) {
            isSet = true
            score += 1
            cardsOnTable[cardsOnTable.index(of: firstCard)!] = nil
            cardsOnTable[cardsOnTable.index(of: secondCard)!] = nil
            cardsOnTable[cardsOnTable.index(of: thirdCard)!] = nil
        }
        return isSet
    }
    
    func selectCard(number index: Int) {
        if setOfThreeCards.count == 3 {
            setOfThreeCards.removeAll()
        }
        guard let selectedCard = cardsOnTable[index] else { return }
        if !selectedCard.isMatched {
            selectedCard.isMatched = true
            setOfThreeCards.append(selectedCard)
            cardSelectedHandler?()
        }
    }
    
    func restart() {
        score = 0
        deck = []
        formDeck()
        shuffleDeck()
        cardsOnTable = []
        layOutCardsOnTable()
        setOfThreeCards = []
    }
}

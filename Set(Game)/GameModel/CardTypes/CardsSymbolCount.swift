extension Card {
    enum CardsSymbolCount: Int {
        case one = 0
        case two
        case three
        
        static var allCases: [CardsSymbolCount] = [.one, .two, .three]
    }
}

extension Card {
    enum CardsPatternType: Int {
        case outline = 0
        case filled
        case striped
        
        static var allCases:[CardsPatternType] = [.outline, .filled, .striped]
    }
}

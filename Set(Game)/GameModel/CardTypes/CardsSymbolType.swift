extension Card {
    enum CardsSymbolType: Int {
        case ellipse = 0
        case rhombus
        case wave
        
        static var allCases: [CardsSymbolType] = [.ellipse, .rhombus, .wave]
    }
}

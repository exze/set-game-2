extension Card {
    enum CardsColor: Int {
        case red = 0
        case green
        case purple
        
        static var allCases:[CardsColor] = [.red, .green, .purple]
    }
}
